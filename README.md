## Docker + NodeJs = SoftEng L4

# Node app 

1. Install docker on Win 10 PRO or Linux
	- under windows you need to enable Hyper-V
2. Enable CPU virtualization in BIOS
3. sudo apt get npm
4. sudo apt get nodejs
5. sudo apt install node-express-generator
6. express softengApp
7. cd softengApp
8. npm install
9. cd bin
10. node www

# Docker files

1. Create Dockerfile and .dockerignore
2. See both files as an example
3. docker build -t kisnandor2/softengapp .
4. docker run -p 8080:3000 -d kisnandor2/softengapp
5. visit localhost:8080
6. docker ps
7. docker stop [imageFile]